# Test DevOps

[![pipeline status](https://gitlab.com/mikamboo/test-devops/badges/master/pipeline.svg)](https://gitlab.com/mikamboo/test-devops/-/commits/master)
[![coverage report](https://gitlab.com/mikamboo/test-devops/badges/master/coverage.svg)](https://gitlab.com/mikamboo/test-devops/-/commits/master)

## Start DEV

```bash
# Start node app + mongodb
docker-compose up

# Run jest tests
docker-compose exec app npm test
```

## Self managed mongo database

Login as super user 

```bash
mongo -u roo -p
```

Create user with "readWrite" acces to `promos` database

```js
> use promos
> db.createUser({user: "appuser", pwd: passwordPrompt(), roles: ["readWrite"]})
```

## Heoroku build and deploy image

```bash
heroku login

APP=test-devops-mike

# Push image to registry 
# Prod secrets "Config Vars" provided at runtime (cf https://dashboard.heroku.com/apps/test-devops-mike/settings)
heroku container:push web --app $APP --arg NODE_ENV=production

# Deploy release
heroku container:release web --app $APP
```

### Réflexions

* Docker pour la portabilité de l'env d'exéc (iso-prod)
* Monitoring Withebox : Dévélopper une couche non invasive façon express midleware
* Création de compteur personnalié en mode middlewar pour chaque route
* Gestion conditionnel du build Docker et maintien d'un seul Dockerfile ou héritage
* Ne pas exposer le endpoint `/metrics` (Kube le fait bien via services et ingress)
* Docker image versioning et choix du registry
* CI utiliser heroku cli (plus simple que les appel API ?)
* Existe t-il un service mongodb gratuit en remplacement de mlab ?
* A prpos la BDD pour les branches de DEV ? Automatiser l'istallation d'un backup ?
* Automatiser la copise de la BDD de PROD en PRE-PROD
* Heroku la fonction "PROMOTE TO PROD" ne fonctionne pas avec les app du container registry
