"use strict";

const _ = require("lodash");
const {example} = require("../setup/example");

const {
  match,
} = require("../../src/rules");

describe("Rules", () => {
  example([
    {
      describe: "'gt' checks that the payload is a number greater than expected",
      rule: ["gt", 25],
      payload: 42,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'gt' matches if payload === expected",
      rule: ["gt", 25],
      payload: 25,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'gt' rejects if payload is not a number",
      rule: ["gt", 25],
      payload: "toto",
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=toto is not a number"],
    },
    {
      describe: "'gt' rejects if payload is lesser than expected",
      rule: ["gt", 25],
      payload: 10,
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=10 is lesser than 25"],
    },
    {
      describe: "'lt' checks that the payload is a number lesser than expected",
      rule: ["lt", 25],
      payload: 15,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'lt' matches if payload === expected",
      rule: ["lt", 25],
      payload: 25,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'lt' rejects if payload is not a number",
      rule: ["lt", 25],
      payload: "toto",
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=toto is not a number"],
    },
    {
      describe: "'lt' rejects if payload is greater than expected",
      rule: ["lt", 25],
      payload: 26,
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=26 is greater than 25"],
    },
    {
      describe: "'eq' checks that the payload is equal to expected",
      rule: ["eq", 25],
      payload: 25,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'eq' works with strings",
      rule: ["eq", "toto"],
      payload: "toto",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'eq' rejects if payload is not equal to expected",
      rule: ["eq", 25],
      payload: 26,
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=26 is not equal to 25"],
    },
    {
      describe: "'before' checks that the payload is before expected date",
      rule: ["before", "2018-05-02"],
      payload: "2017-11-24T16:17:12.190Z",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'before' matches if payload is the same day as expected",
      rule: ["before", "2018-05-02"],
      payload: "2018-05-02T16:17:12.190Z",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'before' rejects if payload is not a date",
      rule: ["before", "2018-05-02"],
      payload: "toto",
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=toto is not a valid date"],
    },
    {
      describe: "'after' checks that the payload is after expected date",
      rule: ["after", "2018-05-02"],
      payload: "2018-11-24T16:17:12.190Z",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'after' matches if payload is the same day as expected",
      rule: ["after", "2018-05-02"],
      payload: "2018-05-02T16:17:12.190Z",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'after' rejects if payload is not a date",
      rule: ["after", "2018-05-02"],
      payload: "toto",
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context=toto is not a valid date"],
    },
    {
      describe: "'or' checks that any one of the rules is valid",
      rule: ["or", ["eq", "toto"], ["eq", "titi"]],
      payload: "toto",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'or' checks that any one of the rules is valid",
      rule: ["or", ["eq", "toto"], ["eq", "titi"]],
      payload: "titi",
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'or' rejects if all rules are invalid",
      rule: ["or", ["eq", "toto"], ["eq", "titi"]],
      payload: "tata",
      context: ".my.context",
      isOk: false,
      errorMessages: [
        ".my.context=tata is not equal to toto",
        ".my.context=tata is not equal to titi",
      ],
    },
    {
      describe: "'and' checks that all of the rules are valid",
      rule: ["and", ["gt", 10], ["lt", 15]],
      payload: 12,
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'and' rejects if any rule is invalid",
      rule: ["and", ["gt", 10], ["lt", 15]],
      payload: 9,
      context: ".my.context",
      isOk: false,
      errorMessages: [
        ".my.context=9 is lesser than 10",
      ],
    },
    {
      describe: "'and' rejects if any rule is invalid",
      rule: ["and", ["gt", 10], ["lt", 15]],
      payload: 16,
      context: ".my.context",
      isOk: false,
      errorMessages: [
        ".my.context=16 is greater than 15",
      ],
    },
    {
      describe: "'prop' checks the rule on a sub-path of payload",
      rule: [".description", ["eq", "toto"]],
      payload: {description: "toto"},
      context: ".my.context",
      isOk: true,
      errorMessages: [],
    },
    {
      describe: "'prop' rejects if sub-path does not exists in payload",
      rule: [".description", ["eq", "toto"]],
      payload: {tata: "toto"},
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context={\"tata\":\"toto\"} has not property description"],
    },
    {
      describe: "'prop' rejects if rule does not match sub-path",
      rule: [".description", ["eq", "tata"]],
      payload: {description: "toto"},
      context: ".my.context",
      isOk: false,
      errorMessages: [".my.context.description=toto is not equal to tata"],
    },
  ], async ({rule, payload, context, isOk, errorMessages}) => {
    try {
      const ok = await match({
        rule,
        payload,
        context,
      });
      expect(ok).toEqual(isOk);
    } catch (error) {
      expect(_.pick(error, "status", "messages")).toEqual({
        status: "RuleViolation",
        messages: errorMessages,
      });
    }
  });
});
