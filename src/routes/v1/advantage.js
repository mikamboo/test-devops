"use strict";

const _ = require("lodash");
const express = require("express");
const {ajv} = require("../../utils/ajv");
const initPromosService = require("../../services/promos");
const initWeatherService = require("../../services/weather");
const {wrapAsyncRoute} = require("../../utils/route");

module.exports = async (app, config) => {
  const router = express.Router();
  const promosService = await initPromosService({config});
  const weatherService = initWeatherService({config});

  const checkAdvantageSchema = app.get("api.v1.swagger").definitions.CheckAdvantageRequest;
  const validateCheckAdvantageRequest = ajv.compile(checkAdvantageSchema);
  router
    .post("/", wrapAsyncRoute(async (req, res) => {
      const validRequest = validateCheckAdvantageRequest(req.body);
      if (!validRequest) {
        throw {
          status: 400,
          message: _.map(
            validateCheckAdvantageRequest.errors,
            ({dataPath, message}) => `${dataPath || "request"} ${message}`,
          ),
        };
      }

      const {
        promocode_name: promo,
        arguments: payload,
      } = req.body;

      const meteo = await weatherService.getCurrent(payload.meteo);
      const {advantage} = await promosService.validate({
        promo,
        payload: {
          ...payload,
          meteo,
          date: new Date(),
        },
      });
      res.json({
        status: "Ok",
        advantage,
      });
    }));

  return router;
};
