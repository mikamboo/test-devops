"use strict";

const _ = require("lodash");
const express = require("express");

module.exports = async (app, config) => {
  const router = express.Router();

  router.get("/", (req, res) => {
    res.json({
      docs: "./docs",
      advantage: "./advantage",
      promos: "./promos",
    });
  });

  /* eslint-disable global-require */
  require("../../middlewares/logger")(router, {api: "v1"}, config);

  router.use("/docs", await require("./docs")(app, config));
  router.use("/advantage", await require("./advantage")(app, config));
  router.use("/promos", await require("./promos")(app, config));

  return router;
};
