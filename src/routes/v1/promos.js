"use strict";

const _ = require("lodash");
const express = require("express");
const initPromosService = require("../../services/promos");
const {wrapAsyncRoute} = require("../../utils/route");

module.exports = async (app, config) => {
  const promosService = await initPromosService({config});

  const router = express.Router();
  router
    .post("/", wrapAsyncRoute(async (req, res) => {
      const {
        promocode_name: promo,
        advantage,
        rule,
      } = req.body;
      await promosService.add({promo, advantage, rule});
      res.status(201).end();
    }));

  return router;
};
