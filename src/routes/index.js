"use strict";

const _ = require("lodash");
const express = require("express");
const monitor = require("prom-client");
const promBundle = require("express-prom-bundle");

const metricsMiddleware = promBundle({includeMethod: true});

/**
 * Pormotheus collect server metrics
 */
const collectDefaultMetrics = monitor.collectDefaultMetrics;
collectDefaultMetrics({timeout: 5000});

module.exports = async (app, config) => {
  const {codeVersion, baseUrl} = config;

  app.use(metricsMiddleware);

  const router = express.Router();
  app.use(baseUrl, router);

  router.get("/", (req, res) => {
    res.json({
      description: "API Description",
      codeVersion,
      v1: "./v1",
    });
  });

  /* eslint-disable global-require */
  router.use("/v1", await require("./v1")(app, config));
};
