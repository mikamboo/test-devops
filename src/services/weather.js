"use strict";

const _ = require("lodash");
const axios = require("axios");

module.exports = ({config}) => {
  const {url, apiKey} = config.openWeather;

  return {
    getCurrent,
  };

  async function getCurrent({town}) {
    const options = {
      method: "GET",
      url,
      params: {
        q: town,
        appid: apiKey,
        units: "metric",
      },
    };

    try {
      const {data} = await axios(options);
      return {
        description: _.get(data, "weather[0].description", "N/A"),
        temp: _.get(data, "main.temp", "N/A"),
      };
    } catch (error) {
      // we should probably improve error feedback here.
      // at least it will do a 500 on the API
      throw error;
    }
  }
};
