"use strict";

const _ = require("lodash");
const {match} = require("../rules");
const {AppError} = require("../utils/error");
const {MongoClient} = require("mongodb");

module.exports = async ({config}) => {
  const {url} = config.mongo;

  const client = await MongoClient.connect(url);
  const db = client.db();
  const promos = db.collection("promos");

  return {
    add,
    validate,
  };

  async function add({promo, advantage, rule}) {
    promos.insert({
      promo,
      advantage,
      rule,
    });
  }

  async function validate({promo, payload}) {
    const promoModel = await promos.findOne({ promo });
    if (!promoModel) {
      throw new AppError({
        status: "NotFound",
        messages: [`Promotion code '${promo}' does not exists`],
      });
    }
    const {rule, advantage} = promoModel;
    const ok = await match({
      rule,
      payload,
    });
    return {
      ok,
      advantage,
    };
  }
};
