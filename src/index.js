"use strict";

const _ = require("lodash");
const config = require("./config");
const initApp = require("./app");

(async () => {
  const app = await initApp(config);

  app.listen(config.port, () => {
    console.log(`HTTP Server started on port ${config.port}`);
  });
})().catch((error) => {
  console.error(error, `Failed to start app: ${error.message}`);
  process.exit(1);
});
