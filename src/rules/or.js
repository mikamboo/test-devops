"use strict";

const _ = require("lodash");
const {AppError} = require("../utils/error");

module.exports = ({match}) => ({
  isHandlerFor(type) {
    return type === "or";
  },
  payloadIsValid: async ({payload, args, context}) => {
    const {result: allResult, errors: allErrors} = await _.reduce(args, (promise, rule) => {
      return promise.then(({result, errors}) => {
        if (result) {
          return {result, errors};
        }
        return match({rule, payload, context})
          .then(() => ({
            result: true,
            errors,
          }), (error) => ({
            result: false,
            errors: [...errors, error],
          }));
      });
    }, Promise.resolve({result: false, errors: []}));
    if (!allResult) {
      throw new AppError({
        status: "RuleViolation",
        messages: _.map(allErrors, (e) => e.message),
      });
    }
    return allResult;
  },
});
