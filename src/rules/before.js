"use strict";

const _ = require("lodash");
const moment = require("moment");
const {AppError} = require("../utils/error");

module.exports = {
  isHandlerFor(type) {
    return type === "before";
  },
  payloadIsValid({payload, args, context}) {
    const value = moment(payload);
    if (!value.isValid()) {
      throw new AppError({status: "RuleViolation", messages: [`${context}=${payload} is not a valid date`]});
    }

    const [expected] = args;
    if (!value.isBefore(moment(expected).add("days", 1))) {
      throw new AppError({status: "RuleViolation", messages: [`${context}=${value} is not before ${expected}`]});
    }

    return true;
  },
};
