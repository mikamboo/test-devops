"use strict";

const _ = require("lodash");

const RulesModels = [
  /* eslint-disable global-require */
  require("./after"),
  require("./and")({match}),
  require("./before"),
  require("./eq"),
  require("./gt"),
  require("./lt"),
  require("./or")({match}),
  require("./prop")({match}),
];

module.exports = {
  match,
};

async function match({rule, payload, context = ""}) {
  const [type, ...args] = rule;
  const ruleModel = _.find(RulesModels, ({isHandlerFor}) => isHandlerFor(type));
  return ruleModel.payloadIsValid({type, payload, args, context});
}
