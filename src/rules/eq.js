"use strict";

const _ = require("lodash");
const {AppError} = require("../utils/error");

module.exports = {
  isHandlerFor(type) {
    return type === "eq";
  },
  payloadIsValid({payload, args, context}) {
    const [expected] = args;
    if (payload !== expected) {
      throw new AppError({
        status: "RuleViolation",
        messages: [`${context}=${payload} is not equal to ${expected}`],
      });
    }

    return true;
  },
};
