"use strict";

const _ = require("lodash");
const {AppError} = require("../utils/error");

module.exports = ({match}) => ({
  isHandlerFor(type) {
    return _.startsWith(type, ".");
  },
  payloadIsValid: async ({type, payload, args, context}) => {
    const prop = type.slice(1);
    const value = _.get(payload, prop);
    if (value === undefined) {
      throw new AppError({
        status: "RuleViolation",
        messages: [`${context}=${JSON.stringify(payload)} has not property ${prop}`]});
    }
    const [rule] = args;
    return match({
      rule,
      payload: value,
      context: `${context}${type}`,
    });
  },
});
