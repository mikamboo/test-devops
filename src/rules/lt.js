"use strict";

const _ = require("lodash");
const {AppError} = require("../utils/error");

module.exports = {
  isHandlerFor(type) {
    return type === "lt";
  },
  payloadIsValid({payload, args, context}) {
    const value = parseInt(payload, 10);
    if (_.isNaN(value)) {
      throw new AppError({status: "RuleViolation", messages: [`${context}=${payload} is not a number`]});
    }

    const [expected] = args;
    if (value > expected) {
      throw new AppError({status: "RuleViolation", messages: [`${context}=${value} is greater than ${expected}`]});
    }

    return true;
  },
};
