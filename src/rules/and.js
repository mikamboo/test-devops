"use strict";

const _ = require("lodash");

module.exports = ({match}) => ({
  isHandlerFor(type) {
    return type === "and";
  },
  payloadIsValid: async ({payload, args, context}) => {
    const allMatches = await Promise.all(_.map(args, (rule) => match({rule, payload, context})));
    return _.every(allMatches);
  },
});
