"use strict";

const _ = require("lodash");
const convict = require("convict");

const config = convict({
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV",
  },
  port: {
    doc: "The HTTP server port.",
    format: "port",
    default: 3000,
    env: "PORT",
  },
  baseUrl: {
    doc: "The API base URL.",
    format: "*",
    default: "/",
    env: "BASE_URL",
  },
  codeVersion: {
    doc: "The source code version.",
    format: "*",
    default: "dev",
    env: "CODE_VERSION",
  },
  headers: {
    requestId: {
      doc: "Header used to forward request ids.",
      format: "*",
      default: "x-api-req-id",
      env: "HEADERS_REQ_ID",
    },
  },
  mongo: {
    url: {
      doc: "MongoDB connection URL.",
      format: "*",
      default: "mongodb://localhost:27017/promos",
      env: "MONGODB_URL",
    },
  },
  openWeather: {
    url: {
      doc: "OpenWeather API endpoint.",
      format: "*",
      default: "http://api.openweathermap.org/data/2.5/weather",
      env: "OPENWEATHER_URL",
    },
    apiKey: {
      doc: "Key to use OpenWeather API",
      format: "*",
      default: "",
      env: "OPENWEATHER_KEY",
    },
  },
});

config.validate({allowed: "strict"});

module.exports = config.getProperties();
