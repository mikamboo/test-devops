"use strict";

const _ = require("lodash");
const express = require("express");

module.exports = async (config) => {
  console.log("App starting with config", config);

  const app = express();

  /* eslint-disable global-require */
  await require("./middlewares")(app, config);
  await require("./routes")(app, config);
  // error handler MUST be registered last
  await require("./middlewares/error")(app, config);

  return app;
};
