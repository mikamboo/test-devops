"use strict";

const _ = require("lodash");

const STATUS_CODES = {
  BadRequest: 400,
  NotFound: 404,
  RuleViolation: 422,
  InternalServerError: 500,
};

module.exports = (app) => {
  app.use((err, req, res, _next_) => {
    console.error(err);
    const status = err.status || "InternalServerError";
    res.status(_.get(STATUS_CODES, status, 500));
    return res.json({
      status,
      messages: _.isArray(err.messages) ? err.messages : [err.message],
    });
  });
};
