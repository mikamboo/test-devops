"use strict";

const _ = require("lodash");
const uuid = require("uuid/v4");

module.exports = (router, props, config) => {
  const reqIdHeader = _.get(config, "headers.requestId");

  router.use((req, res, next) => {
    req.id = req.headers[reqIdHeader] || uuid();
    res.set(reqIdHeader, req.id);

    next();
  });
};
