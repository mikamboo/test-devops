FROM node:14-alpine

RUN adduser -D appuser

# Create app directory
WORKDIR /usr/src/app
RUN chown -R appuser:appuser /usr/src/app

ARG NODE_ENV=development
ARG MONGODB_URL
ARG OPENWEATHER_KEY
ARG CODE_VERSION

USER appuser

# Install app dependencies (ensure both package.json AND package-lock.json are copied)
COPY package*.json ./

# If building production we RUN "npm ci --only=production"
RUN if [ "$NODE_ENV" = "production" ]; then npm ci --only=production; else npm install; fi

# Bundle app source
COPY ./src ./src

EXPOSE 3000

CMD [ "npm", "start" ]