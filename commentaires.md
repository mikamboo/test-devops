## Exercice

### 1. Mettre en place une solution d'intégration continue

Faire tourner les tests + le linter sur toutes les branches de dev et avant chaque merge sur master.
  -> Convention de nomagge pour lancer les test sur branches dev*, master (directive only)
Interdire le merge sur master si les tests ne sont pas verts.
  -> Configuration de Gitlab : Pipelines must succeed (only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds)
Bonus: alerting "CI rouge" sur master.
  -> Mailing par défaut et paramétable, possibilité d'utiliser des hooks HTTP (ex: slack)

### 2. Mettre en place un déploiement automatique

Déploiement automatique de master sur une plateforme de pre-production.
  -> Job deploy basé sur 'merge on master'
Déploiement preproduction -> production.
  -> Gitlanb `when: manual` ?
Déploiment d'applications de test pour chaque branche de dev/pour chaque dev ou pull request.
  -> Widlcard DSN et généartion Igress sous K8S
Penser à la gestion des environnements (config).
  -> Passage de variables d'environnement secrètes

### 3. Monitoring

-> Promotheus withebox + prom-client + midlewares moniring

Monitoring de la santé des serveurs (CPU/RAM/temps de réponse).
Monitoring fonctionnel des routes HTTP (url, status).
Monitoring des erreurs (maximum de context pour pouvoir debugger: id de requete, paramètres).
Bonus: dashboard graphiques + outils d'exploitation des logs.

### 4. Base de donnée

Créer les index nécessaires pour garantir l'unicité des promocodes (par nom).
  -> Créer un script nodejs qui execute les cmd avec mongo client
  -> Nécessité de mise en place systeme de gestion des migration up/down (ORM ?)
Et ceux que vous jugez utiles pour garantir les performances d'accès.
Bonus: prévoir des scripts ou un système de migration pour maintenir les informations de promocode.
